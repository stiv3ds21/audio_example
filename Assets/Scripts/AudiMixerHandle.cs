using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Audio;

public class AudiMixerHandle : MonoBehaviour
{
    [SerializeField] public AudioMixer _masterMixer;
    [SerializeField] [Range(0f, 1f)] [OnValueChanged("ChangeVolumeHandler")]
    private float _volume = 1f;

    private void Start()
    {
        _volume = 1f;
        ChangeVolumeHandler();
    }

    private void ChangeVolumeHandler()
    {
        var dB = _volume > 0 ? 20.0f * Mathf.Log10(_volume) : -80.0f;
        _masterMixer.SetFloat("Volume", dB);
    }
    
    private void GetValueVolume()
    {
        _masterMixer.GetFloat("Volume", out var value);
    }
}
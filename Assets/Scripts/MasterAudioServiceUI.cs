﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MasterAudioServiceUI : MonoBehaviour
{
    [SerializeField] private Button _playArrow;
    [SerializeField] private Button _playBackgroundMusic;
    [SerializeField] private Button _stopBackgroundMusic;
    [SerializeField] private Button _addScene;
    [SerializeField] private Button _addSound1;
    [SerializeField] private Button _addSound2;
    
    private void Awake()
    {
        _playArrow.onClick.AddListener(PlayArrow);
        _playBackgroundMusic.onClick.AddListener(PlayBackgroundMusic);
        _stopBackgroundMusic.onClick.AddListener(StopBackgroundMusic);
        _addScene.onClick.AddListener(AddScene);
        _addSound1.onClick.AddListener( AddSound1Handler);
        _addSound2.onClick.AddListener( AddSound2Handler);
    }

    private void AddSound1Handler()
    {
        MasterAudioService.Main.AddSound1Handler();
    }

    private void AddSound2Handler()
    {
        MasterAudioService.Main.AddSound2Handler();
    }

    private void Update()
    {
        var isLoaded = SceneManager.loadedSceneCount > 1 && SceneManager.GetSceneAt(1).isLoaded;
        _addSound1.gameObject.SetActive(isLoaded);
        _addSound2.gameObject.SetActive(isLoaded);
    }

    private void PlayArrow()
    {
        MasterAudioService.Main.PlayArrow();
    }
    
    private void PlayBackgroundMusic()
    {
        MasterAudioService.Main.PlayBackgroundMusic();
    }

    private void StopBackgroundMusic()
    {
        MasterAudioService.Main.StopBackgroundMusic();
    }
    
    private void AddScene()
    {
        MasterAudioService.Main.AddScene();
    }
}
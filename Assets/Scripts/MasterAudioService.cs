using DarkTonic.MasterAudio;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MasterAudioService : MonoBehaviour
{
    private static MasterAudioService _main;

    public static MasterAudioService Main
    {
        get
        {
            if (_main != null) return _main;
            _main = FindObjectOfType<MasterAudioService>();
            return _main;
        }
    }

    [Button]
    public void PlayArrow()
    {
        MasterAudio.PlaySoundAndForget("arrow");
    }

    [HorizontalGroup("BackgroundMusic"), Button]
    public void PlayBackgroundMusic()
    {
        MasterAudio.StartPlaylist("BackgroundMusicController", "BackgroundMusicInLobby");
    }

    [HorizontalGroup("BackgroundMusic"), Button]
    public void StopBackgroundMusic()
    {
        MasterAudio.StopPlaylist("BackgroundMusicController");
    }

    [Button]
    public void AddScene()
    {
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
    }

    public void AddSound1Handler()
    {
        MasterAudio.PlaySoundAndForget("audio_sound_level_win");
    }

    public void AddSound2Handler()
    {
        MasterAudio.PlaySoundAndForget("audio_sound_modification_click");
    }
}
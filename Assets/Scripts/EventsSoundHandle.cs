using DarkTonic.MasterAudio;
using Sirenix.OdinInspector;
using UnityEngine;

public class EventsSoundHandle : MonoBehaviour
{
    [SerializeField] private string _nameType = "arrow";

    [Button]
    private void PlaySound()
    {
        MasterAudio.PlaySound(_nameType);
    }
}